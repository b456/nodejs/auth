const express=require('express')
const session=require('express-session')

const router = require('./router')

const port = process.env.port || 3000

const app = express()

const passport = require('./lib/passport');

app.use(express.json())
app.use(express.urlencoded({extended:false}))


app.use(session({
    secret:'rahasia',
    resave:false,
    saveUninitialized:false,
}))

app.use(passport.initialize());
app.use(passport.session());


app.set('view engine','ejs')

app.use(router)

app.listen(port,()=>{
    console.log(`Server berjalan di port ${port}`);
})

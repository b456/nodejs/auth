const router = require('express').Router();

const auth = require('./controllers/authControllers');

router.get('/register', auth.registerPage);
router.post('/register', auth.register);
router.get('/login', auth.loginPage);
router.post('/login', auth.login);
router.get('/', (req,res)=>{
    return res.render('index')
});

module.exports = router;
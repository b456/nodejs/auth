const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { use } = require('passport');
const passport = require('passport');
// const passport = require('../lib/passport');
const rahasia = "ini rahasia ga boleh disebar-sebar";

const {User} = require('../models');

const registerPage = (req,res) => {
    return res.render('register');
}

const register = (req,res)=> {
    const username = req.body.username;
    const password = req.body.password;

    const encryptedPassword = bcrypt.hashSync(password,10);

    User.create({
        username: username,
        password: encryptedPassword,
    }).then(()=> {
        return res.redirect('/login');
    });
}

const loginPage = (req,res)=>{
    let messages = '';
    if (req.session){
        if (req.session.messages){
            messages = req.session.messages[0];
            req.session.messages = [];
        }
    }
    return res.render('login',{messages: messages});
}

// const login = passport.authenticate('local', {
//     successRedirect: '/',
//     failureRedirect:'/login',
//     failureMessage: true,
// });

const login = (req,res)=> {
    const username = req.body.username;
    const password = req.body.password;

    User.findOne({
        where: { username:username}
    }).then((user)=>{
        if (!user){
            // return done(null, false, {message:"User not found!"});
            return res.json({message:"User not found!"});
        }

        const isPasswordValid = bcrypt.compareSync(password, user.password);
        if (!isPasswordValid){
            // return done(null, false, {message: 'Wrong password'});
            return res.json({message: 'Wrong password'});
        }

        const accessToken = jwt.sign({
            id:user.id,
            username: user.username,
        },rahasia);

        return res.json({
            id:user.id,
            username: user.username,
            accessToken: accessToken
        })

    });
};

module.exports = {
    registerPage,
    register,
    loginPage,
    login,
}